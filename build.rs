extern crate bindgen;
extern crate pkg_config;

use std::env;
use std::path::PathBuf;

fn bind_pkg(name: &str, version: &str, header: &str) {
    let pkg = pkg_config::Config::new()
        .atleast_version(version).probe(name)
        .expect(format!("{} not found!", name).as_str());

    let mut clang_args = vec![];
    for libdir in &pkg.link_paths {
        let dir = libdir.as_os_str().to_string_lossy().to_string();
        println!("cargo:rustc-link-search={}", dir);
    }
    for lib in &pkg.libs {
        println!("cargo:rustc-link-lib={}", lib);
    }
    for incdir in &pkg.include_paths {
        let dir = incdir.as_os_str().to_string_lossy().to_string();
        clang_args.push(format!("-I{}", dir));
    }
    for (key, value) in &pkg.defines {
        clang_args.push(if let Some(v) = value {
            format!("-D{}={}", key, v)
        } else {
            format!("-D{}", key)
        });
    }

    let bindings = bindgen::Builder::default()
        .header_contents(format!("wrapper_{}.h", name).as_str(), header)
        .clang_args(clang_args)
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join(format!("bindings_{}.rs", name).as_str()))
        .expect("Couldn't write bindings!");
}

fn main() {
    bind_pkg("fluidsynth", "2.1.7", "#include <fluidsynth.h>");
    bind_pkg("opus",       "1.3.1", "#include <opus.h>");
}

