// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2021 Jason Francis <zxzax@protonmail.com>

import {Bot} from './src/mod.ts';

const {default: config} = await import('./config.ts').catch(err => {
  console.error('Failed to open config.ts!');
  console.error(err);
  Deno.exit(1);
});
await Bot.create(config);

