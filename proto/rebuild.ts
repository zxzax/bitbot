// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2021 Jason Francis <zxzax@protonmail.com>

import { path as Path } from '../deps.ts';
import { $ } from '../src/util.ts';

const protodir = Path.dirname(Path.fromFileUrl(Deno.mainModule));
const root = Path.dirname(protodir);
Deno.chdir(root);

const MUMBLE_PROTO = Path.join(protodir, 'Mumble.proto');
const MUMBLE_PROTO_JS = Path.join(protodir, 'Mumble.proto.js');
const MUMBLE_PROTO_TS = Path.join(protodir, 'Mumble.proto.d.ts');

await $`npx pbjs -t static-module -w es6 ${MUMBLE_PROTO} -o ${MUMBLE_PROTO_JS}`;
await $`npx pbts ${MUMBLE_PROTO_JS} -o ${MUMBLE_PROTO_TS}`;
for (const path of [MUMBLE_PROTO_JS, MUMBLE_PROTO_TS]) {
  (async () => {
    let contents = await Deno.readTextFile(path);
    contents = contents.replaceAll('number|Long', 'number');
    contents = contents.replace('import * as $protobuf', 'import { Protobuf as $protobuf }');
    contents = contents.replace('from "protobufjs"', 'from "protobufjs/minimal"');
    contents = contents.replace('protobufjs/minimal', '../deps.ts');
    await Deno.writeTextFile(path, contents);
  })();
}

