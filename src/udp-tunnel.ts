// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2017 Michiel Mooring <michielmooring@gmail.com>
// SPDX-FileCopyrightText: 2020 Jannis Achstetter <kripton@kripserver.net>

import { Opus } from 'https://deno.land/x/opus@0.1.1/mod.ts';

export enum MessageType {
  VoiceCELTAlpha,
  Ping,
  VoiceSpeex,
  VoiceCELTBeta,
  VoiceOpus,
  Max = VoiceOpus
}

type OutgoingVoiceData = {
  type: MessageType;
  target: number;
  sequence: number;
  lastFrame: boolean;
  data: Uint8Array;
};

type IncomingVoiceData = {
  type: MessageType;
  target: number;
  sequence: number;
  lastFrame: boolean;
  sender: number;
  raw: Uint8Array;
  data: Uint16Array;
};

export function encodePacket(encoder: Opus, packet: OutgoingVoiceData): Uint8Array {
  if (packet.type !== MessageType.VoiceOpus)
    throw new Error('Only Opus voice packets are supported');
  if (packet.data.length > 0x1FFF)
    throw new Error(`Audio frame too long! Max Opus length is ${0x1FFF} bytes.`);

  const frameSize = packet.data.length / 2 / encoder.channels;
  const raw = encoder.encode(packet.data, frameSize);
  const typeTarget = ((packet.type & 0x7) << 5) | (packet.target & 0x1F);

  const sequenceVar = toVarInt(packet.sequence);
  const opusHeader = toVarInt(packet.data.length + (packet.lastFrame ? 0x80 : 0));
  const headerLength = 1 + sequenceVar.length + opusHeader.length;

  const frame = new Uint8Array(headerLength + packet.data.length)
  frame[0] = typeTarget;
  frame.set(sequenceVar, 1);
  frame.set(opusHeader, 1 + sequenceVar.length);
  frame.set(raw, headerLength);

  return frame;
}

export function decodePacket(encoder: Opus, packet: Uint8Array): IncomingVoiceData | undefined {
  const type = packet[0] >> 5;
  const target = packet[0] & 0x1F;

  if (type > MessageType.Max)
    throw new Error(`Unknown UDP packet type: ${type}`);
  if (type !== MessageType.VoiceOpus)
    return;
  let offset = 1;

  let varInt = fromVarInt(packet.slice(offset, offset + 9));
  const sender = varInt.value;
  offset += varInt.consumed;

  varInt = fromVarInt(packet.slice(offset, offset + 9));
  const sequence = varInt.value;
  offset += varInt.consumed;

  varInt = fromVarInt(packet.slice(offset, offset + 9));
  const opusHeader = varInt.value;
  offset += varInt.consumed;

  const opusLength = opusHeader & 0x1FFF;
  const lastFrame = (opusHeader & 0x2000) ? true : false;

  const raw = packet.slice(offset, offset + opusLength);
  const data = encoder.decode(raw);

  return {
    type,
    target,
    sender,
    sequence,
    lastFrame,
    raw,
    data,
  };
}

function toVarInt(i: number): Uint8Array {
  const arr = [];
  if (i < 0) {
    i = ~i;
    if (i <= 0x3)
      return new Uint8Array([ 0xFC | i ]);

    arr.push( 0xF8 );
  }

  if (i < 0x80) {
    arr.push(i);
  } else if (i < 0x4000) {
    arr.push((i >> 8) | 0x80);
    arr.push(i & 0xFF);
  } else if (i < 0x200000) {
    arr.push((i >> 16) | 0xC0);
    arr.push((i >> 8) & 0xFF);
    arr.push(i & 0xFF);
  } else if (i < 0x10000000) {
    arr.push((i >> 24) | 0xE0);
    arr.push((i >> 16) & 0xFF);
    arr.push((i >> 8) & 0xFF);
    arr.push(i & 0xFF);
  } else if (i < 0x100000000) {
    arr.push(0xF0);
    arr.push((i >> 24) & 0xFF);
    arr.push((i >> 16) & 0xFF);
    arr.push((i >> 8) & 0xFF);
    arr.push(i & 0xFF);
  } else
    throw new TypeError(`Non-integer values are not supported: ${i}`);

  return new Uint8Array(arr);
}

function fromVarInt(buf: Uint8Array): { value: number, consumed: number } {
  const retVal = {
    value: 0,
    consumed: 0
  };

  if (buf[0] < 0x80) {
    // 0xxxxxxx            7 bit positive number
    retVal.value = buf[0];
    retVal.consumed = 1;
  } else if (buf[0] < 0xC0) {
    // 10xxxxxx + 1 byte   14-bit positive number
    retVal.value = (buf[0] & 0x3F) << 8;
    retVal.value |= buf[1];
    retVal.consumed = 2;
  } else if (buf[0] < 0xE0) {
    // 110xxxxx + 2 bytes  21-bit positive number
    retVal.value = (buf[0] & 0x1F) << 16;
    retVal.value |= (buf[1]) << 8;
    retVal.value |= (buf[2]);
    retVal.consumed = 3;
  } else if (buf[0] < 0xF0) {
    // 1110xxxx + 3 bytes  28-bit positive number
    retVal.value = (buf[0] & 0x0F) << 24;
    retVal.value |= (buf[1]) << 16;
    retVal.value |= (buf[2]) << 8;
    retVal.value |= (buf[3]);
    retVal.consumed = 4;
  } else if (buf[0] < 0xF4) {
    // 111100__ + int (32-bit)
    retVal.value = (buf[1]) << 24;
    retVal.value |= (buf[2]) << 16;
    retVal.value |= (buf[3]) << 8;
    retVal.value |= (buf[4]);
    retVal.consumed = 5;
  } else if (buf[0] < 0xFC) {
    // 111101__ + long (64-bit)
    retVal.value = (buf[1]) << 56;
    retVal.value |= (buf[2]) << 48;
    retVal.value |= (buf[3]) << 40;
    retVal.value |= (buf[4]) << 32;
    retVal.value |= (buf[5]) << 24;
    retVal.value |= (buf[6]) << 16;
    retVal.value |= (buf[7]) << 8;
    retVal.value |= (buf[8]);
    retVal.consumed = 9;
  }

  return retVal;
}

