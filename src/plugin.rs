// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2021 Jason Francis <zxzax@protonmail.com>

mod fluidsynth;
mod opus;

use std::borrow::Cow;
use std::cell::RefCell;
use std::collections::HashMap;
use std::mem::MaybeUninit;
use std::rc::Rc;

use deno_core::error::bad_resource_id;
use deno_core::error::generic_error;
use deno_core::error::AnyError;
use deno_core::op_async;
use deno_core::Extension;
use deno_core::OpState;
use deno_core::Resource;
use deno_core::ResourceId;
use deno_core::ZeroCopyBuf;
use serde::Deserialize;

#[no_mangle]
pub fn init() -> Extension {
    Extension::builder()
        .ops(vec![
             ("op_new_fluid_player", op_async(op_new_fluid_player)),
             ("op_fluid_player_set_soundfonts",
              op_async(op_fluid_player_set_soundfonts)),
             ("op_fluid_player_get_mono_opus_samples",
              op_async(op_fluid_player_get_mono_opus_samples)),
        ])
        .build()
}

struct FluidPlayerState {
    player: fluidsynth::Player,
    encoder: opus::Encoder,
    soundfonts: Vec<i32>,
}

impl FluidPlayerState {
    fn new(options: &HashMap<String, fluidsynth::Setting>
           ) -> Result<FluidPlayerState, AnyError> {
        let mut settings = fluidsynth::Settings::new()?;
        for (key, value) in options {
            settings.set(key, value)?;
        }
        let synth = fluidsynth::Synth::new(settings)?;
        let player = fluidsynth::Player::new(synth)?;
        let encoder = opus::Encoder::new()?;
        Ok(FluidPlayerState {
            player,
            encoder,
            soundfonts: vec![],
        })
    }
}

type FluidPlayerTask = Box<dyn FnOnce(&mut FluidPlayerState) + Send + Sync>;

struct FluidPlayerResource {
    tx: crossbeam_channel::Sender<FluidPlayerTask>
}

impl FluidPlayerResource {
    fn from_rid(state: Rc<RefCell<OpState>>,
                rid: ResourceId
               ) -> Result<Rc<FluidPlayerResource>, AnyError> {
        return state.borrow().resource_table
            .get::<FluidPlayerResource>(rid)
            .ok_or_else(bad_resource_id);
    }

    async fn exec<T, F>(&self, task: F) -> Result<T, AnyError>
        where F: 'static + Send + Sync + FnOnce(&mut FluidPlayerState) -> T,
              T: 'static + Send + Sync + std::fmt::Debug {
        let (dtx, drx) = futures::channel::oneshot::channel();
        self.tx.send(Box::new(move |state| {
            dtx.send(task(state)).ok();
        }))?;
        Ok(drx.await?)
    }
}

impl Resource for FluidPlayerResource {
    fn name(&self) -> Cow<str> {
        "FluidPlayerResource".into()
    }
}

async fn op_new_fluid_player(
    state: Rc<RefCell<OpState>>,
    options: HashMap<String, fluidsynth::Setting>,
    _: ()
    ) -> Result<ResourceId, AnyError> {
    let (tx, rx) = crossbeam_channel::unbounded::<FluidPlayerTask>();
    let (dtx, drx) = futures::channel::oneshot::channel();
    std::thread::spawn(move || {
        let mut player_state = match FluidPlayerState::new(&options) {
            Ok(ps) => {
                if let Err(_) = dtx.send(Ok(())) { return; }
                ps
            },
            Err(e) => {
                dtx.send(Err(e)).ok();
                return;
            }
        };
        loop {
            if let Ok(task) = rx.recv() {
                task(&mut player_state);
            } else {
                break;
            }
        }
    });
    let res = FluidPlayerResource { tx };
    drx.await??;
    Ok(state.as_ref().borrow_mut().resource_table.add(res))
}

async fn op_fluid_player_set_soundfonts(
    state: Rc<RefCell<OpState>>,
    rid: ResourceId,
    soundfonts: Vec<String>,
    ) -> Result<(), AnyError> {
    FluidPlayerResource::from_rid(state, rid)?.exec(move |state| {
        let synth = state.player.mut_synth();
        for id in &state.soundfonts {
            synth.sfunload(*id, true)?;
        }
        state.soundfonts.clear();
        for path in soundfonts {
            let id = synth.sfload(&path, false)?;
            state.soundfonts.push(id);
        }
        Ok(())
    }).await?
}

fn mix_stereo_to_mono(left: &mut [i16; 240], right: &[i16; 240]) {
    for i in 0..left.len() {
        left[i] = left[i].saturating_add(right[i]);
    }
}

async fn op_fluid_player_get_mono_opus_samples(
    state: Rc<RefCell<OpState>>,
    rid: ResourceId,
    args: (ZeroCopyBuf, ZeroCopyBuf),
    ) -> Result<usize, AnyError> {
    let (mut input, mut output) = args;
    if input.len() < 240 * std::mem::size_of::<i16>() {
        return Err(generic_error("Input buffer must be at least 480 bytes"));
    }
    FluidPlayerResource::from_rid(state, rid)?.exec(move |state| {
        let synth = state.player.mut_synth();
        let left = &mut unsafe{ *(input.as_mut_ptr() as *mut [i16; 240]) };
        let mut right_uninit = MaybeUninit::<[i16; 240]>::uninit();
        let right = synth.write_s16(left, &mut right_uninit)?;
        mix_stereo_to_mono(left, right);
        let bytes = state.encoder.encode_mono(left, output.as_mut())?;
        Ok(bytes)
    }).await?
}

