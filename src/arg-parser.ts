// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2021 Jason Francis <zxzax@protonmail.com>

import { wrap } from '../deps.ts';
import * as Util from './util.ts';

type AliasMap = Record<string, string>;

type Options = {
  aliases?: AliasMap;
  help?: {
    enabled?: boolean;
    commandPrefix?: string;
  };
};

interface IRequest {
  readonly rawMessage: string;
  accept(type: 'command' | 'string' | 'rest'): string;
  extract(type: 'command' | 'string' | 'rest'): string;
  get end(): boolean;
}

export class StringRequest implements IRequest {
  protected message: string;
  readonly rawMessage: string;
  constructor(message: string, raw: string) {
    this.message = message;
    this.rawMessage = raw;
  }
  accept(type: 'command' | 'string' | 'rest'): string {
    if (type === 'command')
      return this.message.split(/\s+/, 1)[0];
    else if (type === 'rest') {
      return this.message;
    } else {
      const [str, _length] = Util.getNextQuotedArg(this.message);
      return str;
    }
  }
  extract(type: 'command' | 'string' | 'rest'): string {
    if (this.end)
      throw new TypeError('No arguments left');
    if (type === 'command') {
      const cmd = this.accept(type);
      this.message = this.message.substring(cmd.length).trimStart();
      return cmd;
    } else if (type === 'rest') {
      const rest = this.message;
      this.message = '';
      return rest;
    } else {
      const [str, length] = Util.getNextQuotedArg(this.message);
      this.message = this.message.substring(length).trimStart();
      return str;
    }
  }
  get end(): boolean {
    return this.message.length === 0;
  }
}

export class ArgsRequest implements IRequest {
  protected args: string[];
  readonly rawMessage: string;
  constructor(args: string[], raw: string) {
    this.args = args;
    this.rawMessage = raw;
  }
  accept(type: 'command' | 'string' | 'rest'): string {
    if (type === 'rest')
      return this.args[0] ?? '';
    else
      return this.args[0] ?? '';
  }
  extract(type: 'command' | 'string' | 'rest'): string {
    if (type === 'rest') {
      if (this.args.length > 1)
        throw new TypeError('Too many arguments');
    } else {
      if (this.end)
        throw new TypeError('No arguments left');
    }
    return this.args.shift() ?? '';
  }
  get end(): boolean {
    return this.args.length === 0;
  }
}

export type CommandMap<T> = Record<string, Command<T>>;

type Command<T> = {
  impl: (self: T, ...args: string[]) => AsyncGenerator<string>,
  describe?: string;
  subcommands?: Record<string, Command<T>>;
  arguments?: Argument[];
};

type Argument = {
  name?: string;
  describe?: string;
  type?: 'string' | 'integer' | 'float' | 'string[]' | 'rest';
  required?: boolean;
};

const HELP_COMMAND = {
  describe: 'Show help for a command.',
  arguments: [{ name: 'commands', type: 'string[]' }]
} as const;

type DispatchOptions = {
  maxTextWidth?: number;
};

type HelpOptions = {
  expandRest?: boolean;
  maxWidth?: number;
};

export class ArgParser<T> {
  private readonly commands: CommandMap<T>;
  private readonly aliases: AliasMap;
  private readonly helpEnabled: boolean;
  private readonly helpCmdPrefix: string;

  constructor(commands: CommandMap<T>, options: Options) {
    this.commands = commands;
    this.aliases = Object.assign({}, options.aliases);
    this.helpEnabled = !!options.help?.enabled;
    this.helpCmdPrefix = options.help?.commandPrefix ?? '';
  }
  async *dispatch(self: T, request: IRequest, options?: DispatchOptions): AsyncGenerator<string> {
    const cmd = resolveCommand(this.commands, this.aliases, request);
    if (cmd === (HELP_COMMAND as unknown) && this.helpEnabled) {
      const cmds = extractArg(HELP_COMMAND.arguments[0], 0, request);
      yield this.help(cmds, {
        expandRest: request instanceof ArgsRequest,
        maxWidth: options?.maxTextWidth
      });
      return;
    }
    if (typeof cmd !== 'object')
      throw new Error(`Unknown command "${request.rawMessage}".`);
    const args = (cmd.arguments ?? []).reduce(
      (args: string[], arg, i) => [...args, ...extractArg(arg, i, request)], []);
    const method = cmd.impl;
    for await (const result of method(self, ...args))
      yield result;
  }
  help(commands: string[], options?: HelpOptions): string {
    const raw = commands.join(' ');
    const args = new ArgsRequest(commands, raw);
    const cmd = resolveCommand(this.commands, this.aliases, args);

    let message = '';
    if (!cmd && commands.length)
      message += `Unknown command "${args.rawMessage}".\n\n`;

    const usage = this.helpCmdPrefix + commands.join(' ');
    if (cmd) {
      message += `Usage: ${usage}`;
      if (cmd.subcommands && Object.keys(cmd.subcommands).length) {
        message += '[command]';
      } else {
        message += describeArgs(cmd.arguments ?? [], options) + '\n';
      }
    }

    let map: CommandMap<T> | void;
    if (cmd) {
      map = cmd.subcommands;
    } else {
      message += 'Supported commands:\n';
      map = this.commands;
    }
    if (map) {
      const subcommands = getAllCommands(usage, map);
      message += displayTwoColumn(subcommands, options);
    } else if (cmd?.arguments?.length) {
      message += displayTwoColumn(cmd?.arguments.map((arg, index) => [
        arg.name ?? `arg${index}`, arg.describe ?? ''
      ]), options);
    }
    return message;
  }
}

function getAllCommands<T>(prefix: string,
                              map: CommandMap<T>,
                             ): [string, string][] {
  return [...Object.entries(map)].reduce((cmds, [name, cmd]) => {
    const fullName = prefix.length ? prefix + ' ' + name : name;
    return [
      ...cmds,
      [fullName, cmd.describe ?? ''] as [string, string],
      ...(cmd.subcommands ? getAllCommands(fullName, cmd.subcommands) : [])
    ];
  }, new Array<[string, string]>());
}

function describeArgs(args: Argument[], options?: { expandRest?: boolean }): string {
  const expandRest = options?.expandRest;
  return args.map((arg, index) => {
    const name = arg.name ?? `arg${index}`;
    const desc = arg.required ? name : `[${name}]`;
    if (arg.type === 'string[]' || (arg.type === 'rest' && expandRest))
      return `...${desc}`;
    return desc;
  }).join(' ');
}

function displayTwoColumn(rows: [string, string][],
                          options?: { maxWidth?: number }
                         ): string {
  const SPACING = 4 as const;
  const spacer = ''.padStart(SPACING);
  const nameColWidth = Math.max(...rows.map(([name]) => name.length));
  const maxWidth = Math.max(1, options?.maxWidth ?? Infinity);
  const descColWidth = maxWidth - nameColWidth - SPACING;

  if (descColWidth === Infinity)
    return rows.map(([name, desc]) => {
      if (desc)
        return name + spacer + desc;
      return name;
    }).join('\n');

  if (descColWidth > 0)
    return rows.map(([name, desc]) => {
      if (desc) {
        const indent = spacer + ''.padStart(nameColWidth);
        return name + spacer + wrap(desc, {
          cut: true,
          width: descColWidth,
          indent
        }).trimStart();
      }
      return name;
    }).join('\n');

  const descLineWidth = maxWidth - SPACING;
  if (descLineWidth > 0)
    return rows.map(([name, desc]) => {
      if (desc)
        return name + '\n' + wrap(desc, {
          cut: true,
          width: descLineWidth,
          indent: spacer,
        });
      return name;
    }).join('\n');

  return rows.map(([name, desc]) => {
    if (desc)
      return name + '\n' + desc;
    return name;
  }).join('\n');
}

function extractArg(def: Argument, index: number, request: IRequest): string[] {
  const name = def.name ?? `arg${index}`;
  if (def.required && request.end)
    throw new TypeError(`Missing required argument ${name}`);
  if (def.type === 'rest') {
    if (request.end)
      return [];
    return [request.extract('rest')];
  } else if (def.type === 'string[]') {
    const array = [];
    while (!request.end)
      array.push(request.extract('string'));
    return array;
  }
  const arg = request.extract('string');
  if (def.type === 'float') {
    if (!isFinite(parseFloat(arg)))
      throw new TypeError(`Argument ${name} is not a float: ${arg}`);
  } else if (def.type === 'integer') {
    if (!isFinite(parseInt(arg, 10)))
      throw new TypeError(`Argument ${name} is not an integer: ${arg}`);
  }
  return [arg];
}

function resolveAlias(cmd: string, aliases: AliasMap): string {
  const hitAliases = new Set<string>(); // prevent infinite loops
  while (aliases[cmd] != null && !hitAliases.has(cmd)) {
    hitAliases.add(cmd);
    cmd = aliases[cmd];
  }
  return cmd;
}

function resolveCommand<T>(commands: CommandMap<T>,
                           aliases: AliasMap,
                           request: IRequest,
                           parents: Command<T>[] = []
                          ): Command<T> | undefined {
  const name = resolveAlias(request.accept('command'), aliases);
  if (name === 'help' && !parents.length)
    return HELP_COMMAND as unknown as Command<T>;
  const cmd = commands[name];
  if (cmd) {
    request.extract('command');
    const {subcommands} = cmd;
    if (subcommands) {
      return resolveCommand(subcommands, aliases, request, [...parents, cmd]);
    }
    return cmd;
  }
  return parents[parents.length - 1];
}

