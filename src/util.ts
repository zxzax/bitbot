// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2021 Jason Francis <zxzax@protonmail.com>

export function getNextQuotedArg(message: string): [string, number] {
  let output = '';
  let i = 0;
  for (; i < message.length; i++) {
    if (!message[i].match(/^\s+$/))
      break;
  }
  for (; i < message.length; i++) {
    const c = message[i];
    if (c === '"' || c === '\'') {
      i++;
      for (;; i++) {
        if (i >= message.length)
          throw new Error(`Unclosed quotation mark \`${c}\``);
        const d = message[i];
        if (d === c)
          break;
        output += d;
      }
    } else if (c.match(/^\s+$/)) {
      break;
    } else
      output += c;
  }
  return [output, i];
}

export function shsplit(cmd: string): string[] {
  const args = [];
  cmd = cmd.trim();
  while (cmd.length > 0) {
    const [arg, arglen] = getNextQuotedArg(cmd);
    args.push(arg);
    cmd = cmd.slice(arglen);
  }
  return args;
}

export function shjoin(args: string[]): string {
  return args.map(shescape).join(' ');
}

export function shescape(arg: string): string {
  if (arg.match("'"))
    return `"${arg.replaceAll('"', '"\'"\'"')}"`;
  else if (arg.length === 0 || arg.match(/['\s]/))
    return `'${arg.replaceAll("'", "'\"'\"'")}'`;
  else
    return arg;
}

export function sh(strs: TemplateStringsArray, ...exps: string[]): string {
  return exps.reduce((cmd, exp, i) => cmd + shescape(exp) + strs[i + 1], strs[0]);
}

export async function $(strs: TemplateStringsArray, ...exps: string[]) {
  const cmd = sh(strs, ...exps);
  console.log('>', cmd);
  const p = Deno.run({ cmd: shsplit(cmd) });
  try {
    const status = await p.status();
    if (status.code != 0)
      Deno.exit(status.code);
  } finally {
    p.close();
  }
}

