// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2021 Jason Francis <zxzax@protonmail.com>

import * as Mumble from './mumble.ts';
import { ArgParser, StringRequest } from './arg-parser.ts';
import { IBot, botCommands } from './commands.ts';
import { path as Path } from '../deps.ts';

export type BotConfig = {
  hostname?: string;
  port?: number;
  username?: string;
  password?: string;
  tokens?: string[];
  certFile?: string;
  channel?: string;
  welcomeMessage?: string;
  comment?: string;
  songMounts?: Record<string, string>;
  soundfontMounts?: Record<string, string>;
  defaultSoundfont?: string;
  ffmpegPath?: string;
  ytdlPath?: string;
  chatInterface?: {
    enabled?: boolean;
    bang?: string;
    aliases?: Record<string, string>;
  };
  webInterface?: {
    enabled?: boolean;
    hostname?: string;
    port?: number;
  }
};

const DEFAULT_CLIENT_OPTIONS = Object.freeze({
  username: 'Bitbot',
  hostname: 'localhost',
  port: 64738,
});
const DEFAULT_CHAT_OPTIONS = Object.freeze({
  bang: '!',
  aliases: {}
});
const DEFAULT_WEB_OPTIONS = Object.freeze({
  hostname: 'localhost',
  port: 8090
});

export class Bot implements IBot {
  readonly client: Mumble.Client;
  private readonly webListener?: Deno.Listener;
  private soundfont?: string;
  private songMounts: Record<string, string>;
  private soundfontMounts: Record<string, string>;
  private readonly chatInterface?: ArgParser<IBot>;
  private readonly bang: string;
  private gain = 1;

  static async create(options?: BotConfig) {
    const clientOptions = Object.assign({}, DEFAULT_CLIENT_OPTIONS, options);
    const client = await Mumble.Client.connect(clientOptions);
    const bot = new Bot(client, options);
    await bot.client.modifyUser({
      channel: options?.channel,
      comment: options?.comment
    });
    if (options?.welcomeMessage)
      await bot.client.send(options.welcomeMessage,
                            options.channel ? { channels: [options.channel] } : {});
    return bot;
  }
  constructor(client: Mumble.Client,
              options?: BotConfig) {
    this.client = client;
    this.songMounts = Bot.normalizeMounts(options?.songMounts);
    this.soundfontMounts = Bot.normalizeMounts(options?.soundfontMounts);
    this.soundfont = options?.defaultSoundfont;

    if (options?.webInterface?.enabled !== false) {
      const webOptions = Object.assign(
        {}, DEFAULT_WEB_OPTIONS, options?.webInterface);
      this.webListener = Deno.listen(webOptions);
    }
    if (options?.chatInterface?.enabled !== false) {
      const {bang, aliases} = Object.assign(
        {}, DEFAULT_CHAT_OPTIONS, options?.chatInterface);
      this.bang = bang;
      this.chatInterface = new ArgParser<IBot>(botCommands, {
        aliases,
        help: {
          enabled: true,
          commandPrefix: bang
        }
      });
      client.messageHandler = this.chatDispatch.bind(this);
    } else {
      this.bang = '';
    }
  }
  private async chatDispatch(message: Mumble.Message) {
    if (!this.chatInterface)
      return;
    if (this.bang.length && !message.contents.startsWith(this.bang))
      return;
    const command = message.contents.substring(this.bang.length).trimEnd();
    const request = new StringRequest(command, message.contents);
    try {
      for await (const reply of this.chatInterface.dispatch(this, request))
        this.client.replyTo(message, reply);
    } catch (e) {
      await this.client.send(e instanceof Error ? e.message : `${e}`);
      return;
    }
  }
  private static normalizeMounts(mounts?: Record<string, string>) {
    return [...Object.entries(mounts ?? {})].reduce((acc, [src, target]) => {
      if (Path.isAbsolute(target)) {
        const real = Deno.realPathSync(Path.normalize(target));
        try {
          const stat = Deno.statSync(real);
          if (!stat.isDirectory)
            throw new Error();
        } catch (_e) {
          console.warn(`Could not access directory "${real}"`);
        }
        acc[Path.normalize(Path.join('/', src, '/'))] = real;
      }
      return acc;
    }, {} as Record<string, string>);
  }
  async locateFile(filename: string,
                   type: 'song' | 'soundfont'
                   ): Promise<[string, Deno.File] | undefined> {
    filename = Path.normalize(Path.join('/', filename));
    const mounts = type === 'soundfont' ? this.soundfontMounts : this.songMounts;
    for (const [mount, real] of Object.entries(mounts)) {
      if (filename.startsWith(mount)) {
        const path = Path.join(real, filename.substring(mount.length));
        try {
          return [path, await Deno.open(path, { read: true })];
        } catch (_e) {
          continue;
        }
      }
    }
  }
  async queue(song: string): Promise<[boolean, string]> {
    return await Promise.resolve([false, '']);
  }
  async pause(): Promise<string> {
    return await Promise.resolve('');
  }
  async resume(): Promise<string> {
    return await Promise.resolve('');
  }
  async getPlaying(): Promise<string> {
    return await Promise.resolve('');
  }
  async getQueued(): Promise<string[]> {
    return await Promise.resolve([]);
  }
  async skipForward(): Promise<string> {
    return await Promise.resolve('');
  }
  async clearQueue(): Promise<number> {
    return await Promise.resolve(0);
  }
  async listFiles(dir?: string): Promise<string[]> {
    return await Promise.resolve([]);
  }
  async searchFiles(query?: string): Promise<string[]> {
    return await Promise.resolve([]);
  }
  async setVolume(gain: number): Promise<void> {
    if (!isFinite(gain) || gain == null || gain < 0)
      throw new TypeError(`Invalid gain value: ${gain}`);
    this.gain = gain;
    return await Promise.resolve();
  }
  async getVolume(): Promise<number> {
    return await Promise.resolve(this.gain);
  }
  async setSoundfont(file: string): Promise<[string, number]> {
    const [_p, sf] = await this.locateFile(file, 'soundfont') ?? [];
    if (!sf)
      throw new RangeError(`Soundfont \`${file}\` not found.`);
    try {
      this.soundfont = file;
      const stat = await sf.stat();
      return [file, stat.size];
    } finally {
      sf.close();
    }
  }
  async getSoundfont(): Promise<[string, number]> {
    if (!this.soundfont)
      throw new RangeError('No soundfont selected.');
    const [_p, sf] = await this.locateFile(this.soundfont, 'soundfont') ?? [];
    if (sf) {
      try {
        const stat = await sf.stat();
        return [this.soundfont, stat.size];
      } finally {
        sf.close();
      }
    } else
      return [this.soundfont, -1];
  }
  shutdown() {
    this.client.shutdown();
  }
}
