// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2021 Jason Francis <zxzax@protonmail.com>

import { io, evt } from '../deps.ts';
import { Opus, OpusApplication } from 'https://deno.land/x/opus@0.1.1/mod.ts';
import * as UDPTunnel from './udp-tunnel.ts';
// @deno-types="../proto/Mumble.proto.d.ts"
import { MumbleProto } from '../proto/Mumble.proto.js';

const { Evt } = evt;

const MUMBLE_PROTO_VERSION = [1, 2, 4] as const;
const MUMBLE_CLIENT_NAME = 'DenoMumble';
const PING_INTERVAL = 15 * 1000;
const SAMPLE_RATE = 48000;
const CHANNELS = 1;

export enum ChanACL {
  None            = 0x0,
  Write           = 0x1,
  Traverse        = 0x2,
  Enter           = 0x4,
  Speak           = 0x8,
  MuteDeafen      = 0x10,
  Move            = 0x20,
  MakeChannel     = 0x40,
  LinkChannel     = 0x80,
  Whisper         = 0x100,
  TextMessage     = 0x200,
  MakeTempChannel = 0x400,
  Listen          = 0x800,
  Kick             = 0x10000,
  Ban              = 0x20000,
  Register         = 0x40000,
  SelfRegister     = 0x80000,
  ResetUserContent = 0x100000,

  Cached = 0x8000000,
  All = Write + Traverse + Enter + Speak + MuteDeafen + Move + MakeChannel +
        LinkChannel + Whisper + TextMessage + MakeTempChannel + Listen + Kick +
        Ban + Register + SelfRegister + ResetUserContent
}

enum Whisper {
  None = 0,
  Loopback = 0x1f
}

export type ClientOptions = {
  username: string;
  password?: string;
  tokens?: string[];
};

function encodeVersion(major: number, minor: number, patch: number): number {
  return ((major & 0xffff) << 16) | ((minor & 0xff) << 8) | (patch & 0xff);
}

type MumbleMessage = MumbleProto.Version | MumbleProto.UDPTunnel |
                     MumbleProto.Authenticate | MumbleProto.Ping |
                     MumbleProto.Reject | MumbleProto.ServerSync |
                     MumbleProto.ChannelRemove | MumbleProto.ChannelState |
                     MumbleProto.UserRemove | MumbleProto.UserState |
                     MumbleProto.BanList | MumbleProto.TextMessage |
                     MumbleProto.PermissionDenied | MumbleProto.ACL |
                     MumbleProto.QueryUsers | MumbleProto.CryptSetup |
                     MumbleProto.ContextActionModify |
                     MumbleProto.ContextAction | MumbleProto.UserList |
                     MumbleProto.VoiceTarget | MumbleProto.PermissionQuery |
                     MumbleProto.CodecVersion | MumbleProto.UserStats |
                     MumbleProto.RequestBlob | MumbleProto.ServerConfig |
                     MumbleProto.SuggestConfig;

interface Writer {
  finish(): Uint8Array;
}

interface MumbleMessageClass extends Function {
  encode(message: MumbleMessage | unknown, writer?: Writer): Writer;
  decode(reader: Uint8Array, length?: number): MumbleMessage;
}

type ChannelEvents = [ 'parent', Channel? ] |
                     [ 'name', string ] |
                     [ 'links', Set<Channel> ] |
                     [ 'description', string ] |
                     [ 'temporary', boolean ] |
                     [ 'position', number ] |
                     [ 'maxUsers', number ];

export class Channel extends Evt<ChannelEvents> {
  constructor(channel: MumbleProto.ChannelState, others: Map<number, Channel>) {
    super();
    this.id = channel.channelId;
    this.update(channel, others);
  }
  update(channel: MumbleProto.ChannelState, others: Map<number, Channel>) {
    if (Object.getOwnPropertyDescriptor(channel, 'parent')) {
      const parent = others.get(channel.parent);
      if (parent != this._parent) {
        this._parent = parent;
        this.post(['parent', this._parent]);
      }
    }
    for (const link of [...channel.links, ...channel.linksAdd]) {
      const other = others.get(link);
      if (other && !this._links.has(other)) {
        this._links.add(other);
        this.post(['links', this._links]);
      }
    }
    for (const link of channel.linksRemove) {
      const other = others.get(link);
      if (other && this._links.delete(other))
        this.post(['links', this._links]);
    }
    const KEYS = ['name', 'description', 'temporary', 'position', 'maxUsers'] as const;
    for (const prop of KEYS) {
      if (Object.getOwnPropertyDescriptor(channel, prop)) {
        const newValue = channel[prop];
        const self = this as unknown as Record<string, unknown>;
        const oldValue = self[`_${prop}`];
        if (oldValue !== newValue) {
          self[`_${prop}`] = newValue;
          this.post([prop, newValue] as ChannelEvents);
        }
      }
    }
  }
  readonly id: number;
  private _name = '';
  get name() { return this._name; }
  private _links = new Set<Channel>();
  get links() { return new Set(this._links); }
  private _parent?: Channel;
  get parent() { return this._parent; }
  private _description = '';
  get description() { return this._description; }
  private _temporary = false;
  get temporary() { return this._temporary; }
  private _position?: number;
  get position() { return this._position; }
  private _maxUsers?: number;
  get maxUsers() { return this._maxUsers; }
}

type UserEvents = [ 'userId', number? ] |
                  [ 'actor', User? ] |
                  [ 'name', string ] |
                  [ 'channel', Channel? ] |
                  [ 'mute', boolean ] |
                  [ 'deaf', boolean ] |
                  [ 'suppress', boolean ] |
                  [ 'selfMute', boolean ] |
                  [ 'selfDeaf', boolean ] |
                  [ 'texture', Uint8Array? ] |
                  [ 'pluginIdentity', string ] |
                  [ 'pluginContext', Uint8Array? ] |
                  [ 'comment', string ] |
                  [ 'prioritySpeaker', boolean ] |
                  [ 'recording', boolean ];

export class User extends Evt<UserEvents> {
  constructor(user: MumbleProto.UserState,
              others: Map<number, User>,
              channels: Map<number, Channel>) {
    super();
    this.session = user.session;
    this.update(user, others, channels);
  }
  update(user: MumbleProto.UserState,
         others: Map<number, User>,
         channels: Map<number, Channel>) {
    if (Object.getOwnPropertyDescriptor(user, 'actor')) {
      const actor = others.get(user.actor);
      if (actor != this._actor) {
        this._actor = actor;
        this.post(['actor', this._actor]);
      }
    }
    if (Object.getOwnPropertyDescriptor(user, 'channel')) {
      const channel = channels.get(user.channelId);
      if (channel != this._channel) {
        this._channel = channel;
        this.post(['channel', this._channel]);
      }
    }
    const KEYS = ['userId', 'name', 'mute', 'deaf', 'suppress',
                  'selfMute', 'selfDeaf', 'texture', 'pluginIdentity',
                  'pluginContext', 'comment', 'prioritySpeaker', 'recording'] as const;
    for (const prop of KEYS) {
      if (Object.getOwnPropertyDescriptor(user, prop)) {
        const newValue = user[prop];
        const self = this as unknown as Record<string, unknown>;
        const oldValue = self[`_${prop}`];
        if (oldValue !== newValue) {
          self[`_${prop}`] = newValue;
          this.post([prop, newValue] as UserEvents);
        }
      }
    }
  }
  private _userId?: number;
  get id() { return this._userId; }
  readonly session: number;
  private _actor?: User;
  get actor() { return this._actor; }
  private _name = '';
  get name() { return this._name; }
  private _channel?: Channel;
  get channel() { return this._channel; }
  private _mute = false;
  get mute() { return this._mute; }
  private _deaf = false;
  get deaf() { return this._deaf; }
  private _suppress = false;
  get suppress() { return this._suppress; }
  private _selfMute = false;
  get selfMute() { return this._selfMute; }
  private _selfDeaf = false;
  get selfDeaf() { return this._selfDeaf; }
  private _texture?: Uint8Array;
  get texture() { return this._texture; }
  private _pluginIdentity = '';
  get pluginIdentity() { return this._pluginIdentity; }
  private _pluginContext?: Uint8Array;
  get pluginContext() { return this._pluginContext; }
  private _comment = '';
  get comment() { return this._comment; }
  private _prioritySpeaker = false;
  get prioritySpeaker() { return this._prioritySpeaker; }
  private _recording = false;
  get recording() { return this._recording; }
}

export class Message {
  constructor(message: MumbleProto.TextMessage,
              users: Map<number, User>,
              channels: Map<number, Channel>) {
    this.contents = message.message;
    this.sender = users.get(message.actor);
    this.recipients = new Set(message.session.map(s => users.get(s))
                                     .filter((u): u is User => !!u));
    this.channels = new Set(message.channelId.map(i => channels.get(i))
                                     .filter((c): c is Channel => !!c));
    this.trees = new Set(message.treeId.map(i => channels.get(i))
                                     .filter((c): c is Channel => !!c));
  }
  readonly contents: string;
  readonly sender?: User;
  readonly recipients: Set<User>;
  readonly channels: Set<Channel>;
  readonly trees: Set<Channel>;
}

type UserStateOptions = {
  name?: string,
  selfMute?: boolean,
  selfDeaf?: boolean,
  comment?: string,
  channel?: Channel | string,
};

type MessageOptions = {
  recipients?: Iterable<User | string>;
  channels?: Iterable<Channel | string>;
  trees?: Iterable<Channel | string>;
};

export type UDPMessageType = UDPTunnel.MessageType;
export const UDPMessageType = UDPTunnel.MessageType;

type OutgoingVoiceData = {
  type: UDPMessageType;
  target?: Whisper;
  sequence?: number;
  lastFrame?: boolean;
  data: Uint8Array;
};

type IncomingVoiceData = {
  type: UDPMessageType;
  target?: Whisper | User;
  sequence: number;
  lastFrame: boolean;
  sender?: User;
  raw: Uint8Array;
  data: Uint16Array;
};

export class Client {
  private connection?: Deno.Conn;
  private error?: Error;
  private pingInterval: number;
  private _channels = new Map<number, Channel>();
  get channels() { return [...this._channels.values()]; }
  private _users = new Map<number, User>();
  get users() { return [...this._users.values()]; }
  private ready? = new Evt<Error | null>();
  private _user?: User;
  get user(): User {
    if (!this._user)
      throw new Error('Not fully connected');
    return this._user;
  }
  private encoder = new Opus(SAMPLE_RATE, CHANNELS, OpusApplication.VOIP);
  private voiceSequence = 0;

  private _messageHandler?: (message: Message) => void;
  set messageHandler(handler: (message: Message) => void) {
    if (!this.connection)
      throw new Error('Connection closed');
    this._messageHandler = handler;
  }
  private _voiceHandler?: (voiceData: IncomingVoiceData) => void;
  set voiceHandler(handler: (voice: IncomingVoiceData) => void) {
    if (!this.connection)
      throw new Error('Connection closed');
    this._voiceHandler = handler;
  }

  private static MESSAGE_TYPES = new Map<number, MumbleMessageClass>([
    [0, MumbleProto.Version],
    [1, MumbleProto.UDPTunnel],
    [2, MumbleProto.Authenticate],
    [3, MumbleProto.Ping],
    [4, MumbleProto.Reject],
    [5, MumbleProto.ServerSync],
    [6, MumbleProto.ChannelRemove],
    [7, MumbleProto.ChannelState],
    [8, MumbleProto.UserRemove],
    [9, MumbleProto.UserState],
    [10, MumbleProto.BanList],
    [11, MumbleProto.TextMessage],
    [12, MumbleProto.PermissionDenied],
    [13, MumbleProto.ACL],
    [14, MumbleProto.QueryUsers],
    [15, MumbleProto.CryptSetup],
    [16, MumbleProto.ContextActionModify],
    [17, MumbleProto.ContextAction],
    [18, MumbleProto.UserList],
    [19, MumbleProto.VoiceTarget],
    [20, MumbleProto.PermissionQuery],
    [21, MumbleProto.CodecVersion],
    [22, MumbleProto.UserStats],
    [23, MumbleProto.RequestBlob],
    [24, MumbleProto.ServerConfig],
    [25, MumbleProto.SuggestConfig]
  ]);
  private static MESSAGE_IDS = new Map<MumbleMessageClass, number>(
    [...Client.MESSAGE_TYPES.entries()].map(([k, v]) => [v, k]));

  static async connect(options: ClientOptions & Deno.ConnectTlsOptions) {
    await Opus.load();
    const conn = await Deno.connectTls(options);
    const client = new Client(conn);
    await client.write(new MumbleProto.Version({
      version: encodeVersion(...MUMBLE_PROTO_VERSION),
      release: MUMBLE_CLIENT_NAME,
      os: MUMBLE_CLIENT_NAME,
      osVersion: MUMBLE_CLIENT_NAME
    }));
    await client.write(new MumbleProto.Authenticate({
      username: options.username,
      password: options.password,
      tokens: options.tokens,
      opus: true,
    }));
    await new Promise<void>((resolve, reject) => {
      if (client.ready)
        client.ready.attachOnceExtract(error => error ? reject(error) : resolve());
      else if (client.error)
        reject(client.error);
      else
        reject(new Error('Failed'));
    });
    return client;
  }
  private constructor(connection: Deno.Conn) {
    this.connection = connection;
    this.pingInterval = setInterval(this.doPing.bind(this), PING_INTERVAL);
    this.loop();
  }
  private async write(message: MumbleMessage) {
    if (!this.connection) {
      if (this.error)
        throw this.error;
      else
        throw new Error('Connection is closed');
    }
    const type = Object.getPrototypeOf(message).constructor;
    const typeId = Client.MESSAGE_IDS.get(type);
    if (typeof typeId === 'undefined')
      throw new Error(`Invalid packet type: ${type.name}`);
    const packet = type.encode(message).finish();
    const header = new Uint8Array(6);
    const view = new DataView(header.buffer);
    view.setUint16(0, typeId, false);
    view.setUint32(2, packet.length, false);
    await this.connection.write(header);
    await this.connection.write(packet);
  }
  private async doPing() {
    if (!this.connection)
      return;
    try {
      await this.write(new MumbleProto.Ping({ timestamp: Date.now() }));
    } catch (e) {
      this.shutdown(e);
    }
  }
  private async loop() {
    if (!this.connection)
      return;
    try {
      for await (let chunk of io.iter(this.connection)) {
        while (chunk.length > 6) {
          const view = new DataView(chunk.buffer);
          const typeId = view.getUint16(0, false);
          const length = view.getUint32(2, false);
          if (chunk.length < length + 6) {
            this.shutdown(new Error(`Short packet (length ${chunk.length}, expected ${length + 6})`));
            return;
          }
          const buf = chunk.slice(6, length + 6);
          const type = Client.MESSAGE_TYPES.get(typeId);
          if (!type) {
            this.shutdown(new Error(`Unknown message type: ${typeId}`));
            return;
          }
          if (type === MumbleProto.UDPTunnel) {
            this.processUDPTunnel(buf);
          } else {
            const methodName = `process${type.name}`;
            type MumbleCallbacks = Record<string, ((message: MumbleMessage) => void)>;
            const method = (this as unknown as MumbleCallbacks)[methodName];
            if (method)
              method.call(this, type.decode(buf));
          }
          chunk = chunk.slice(buf.length + 6);
        }
        if (!this.connection)
          return;
      }
    } catch (e) {
      this.shutdown(e);
    }
  }
  async writeAudio(packet: OutgoingVoiceData) {
    const sequence = packet.sequence ?? this.voiceSequence;
    const frame = UDPTunnel.encodePacket(this.encoder, {
      type: packet.type,
      target: packet.target ?? 0,
      sequence,
      lastFrame: packet.lastFrame ?? false,
      data: packet.data
    });
    if (sequence >= this.voiceSequence)
      this.voiceSequence = sequence + 1;
    await this.write(new MumbleProto.UDPTunnel({ packet: frame }));
  }
  private processUDPTunnel(packet: Uint8Array) {
    if (!this._voiceHandler)
      return;

    let voiceData;
    try {
      voiceData = UDPTunnel.decodePacket(this.encoder, packet);
    } catch (e) {
      console.warn(e);
      return;
    }
    if (!voiceData)
      return;

    this._voiceHandler(Object.assign({
      sender: this._users.get(voiceData.sender),
    }));
  }
  private processServerSync(sync: MumbleProto.ServerSync) {
    this._user = this._users.get(sync.session);
    if (this.ready) {
      this.ready.post(null);
      delete this.ready;
    }
  }
  private processUserState(state: MumbleProto.UserState) {
    if (state.session) {
      const user = this._users.get(state.session);
      if (user)
        user.update(state, this._users, this._channels);
      else
        this._users.set(state.session, new User(state, this._users, this._channels));
    }
  }
  private processUserRemove(remove: MumbleProto.UserRemove) {
    this._users.delete(remove.session);
  }
  private processChannelState(state: MumbleProto.ChannelState) {
    if (state.channelId) {
      const channel = this._channels.get(state.channelId);
      if (channel)
        channel.update(state, this._channels);
      else
        this._channels.set(state.channelId, new Channel(state, this._channels));
    }
  }
  private processChannelRemove(remove: MumbleProto.ChannelRemove) {
    this._channels.delete(remove.channelId);
  }
  private processTextMessage(message: MumbleProto.TextMessage) {
    if (!this._messageHandler)
      return;
    this._messageHandler(new Message(message, this._users, this._channels));
  }
  findUser(name: string): User | undefined {
    for (const u of this._users.values())
      if (u.name === name)
        return u;
  }
  findChannel(name: string): Channel | undefined {
    for (const c of this._channels.values())
      if (c.name === name)
        return c;
  }
  async send(message: string, options?: MessageOptions) {
    const msg = new MumbleProto.TextMessage({
      message: message,
      session: [...options?.recipients ?? []].map(u => {
        if (typeof u === 'string')
          return this.findUser(u)?.session;
        return u.session;
      }).filter((i): i is number => typeof i !== 'undefined'),
      channelId: [...options?.channels ?? []].map(c => {
        if (typeof c === 'string')
          return this.findChannel(c)?.id;
        return c.id;
      }).filter((i): i is number => typeof i !== 'undefined'),
      treeId: [...options?.trees ?? []].map(c => {
        if (typeof c === 'string')
          return this.findChannel(c)?.id;
        return c.id;
      }).filter((i): i is number => typeof i !== 'undefined'),
    });
    if (!msg.session.length && !msg.channelId.length && !msg.treeId.length &&
        this.user.channel?.id)
      msg.channelId.push(this.user.channel?.id);
    await this.write(msg);
  }
  async replyTo(message: Message, reply: string) {
    const targets = {
      recipients: [] as User[],
      channels: message.channels
    };
    if (!targets.channels.size && message.sender)
      targets.recipients.push(message.sender);
    await this.send(reply, targets);
  }
  async modifyUser(options: UserStateOptions) {
    let channel = options.channel;
    if (typeof channel === 'string') {
      const c = this.findChannel(channel);
      if (!c)
        throw new Error(`Invalid channel name ${channel}`);
      channel = c;
    }
    await this.write(new MumbleProto.UserState(Object.assign(
      {}, options, {
        session: this.user.session,
        actor: this.user.session,
        channelId: channel?.id
      })));
  }
  shutdown(error?: Error) {
    if (this.connection) {
      if (error) {
        console.error('Connection closed unexpectedly:', error);
        this.error = error;
      }
      if (this.ready) {
        this.ready.post(this.error ?? new Error('Connection closed'));
        delete this.ready;
      }
      clearInterval(this.pingInterval);
      this.pingInterval = 0;
      delete this._messageHandler;
      delete this._voiceHandler;
      const {connection} = this;
      delete this.connection;
      connection.close();
      this.encoder.delete();
    }
  }
}

