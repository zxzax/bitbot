// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2021 Jason Francis <zxzax@protonmail.com>

import { CommandMap } from './arg-parser.ts';
import { convertSize } from '../deps.ts';

export interface IBot {
  queue(song: string): Promise<[boolean, string]>;
  pause(): Promise<string>;
  resume(): Promise<string>;
  getPlaying(): Promise<string>;
  getQueued(): Promise<string[]>;
  skipForward(): Promise<string>;
  clearQueue(): Promise<number>;
  listFiles(dir?: string): Promise<string[]>;
  searchFiles(query?: string): Promise<string[]>;
  setVolume(gain: number): Promise<void>;
  getVolume(): Promise<number>;
  setSoundfont(file: string): Promise<[string, number]>;
  getSoundfont(): Promise<[string, number]>;
}

export const botCommands: CommandMap<IBot> = {
  play: {
    describe: 'Play a file or URL, or continue playing after a pause.',
    arguments: [{ name: 'query', type: 'rest' }],
    impl: async function*(bot: IBot, ...[song]: string[]) {
      if (song != null) {
        const [playing, desc] = await bot.queue(song);
        if (playing)
          yield `Now Playing: ${desc}`;
        else
          yield `Queued: ${desc}`;
      } else {
        const desc = await bot.resume();
        yield `Resumed: ${desc}`;
      }
    }
  },
  nowplaying: {
    describe: 'Show the current playing song.'
  },
  queue: {
    describe: 'Add multiple files or URLs to the play queue, or shows the current play queue.'
  },
  pause: {
    describe: 'Pause the currently playing media.'
  },
  skip: {
    describe: 'Skip to the next item in the playlist.'
  },
  stop: {
    describe: 'Stop and clear the current playlist.'
  },
  volume: {
    describe: 'Get or set the audio volume.',
    arguments: [{ name: 'gain', type: 'float' }],
  },
  list: {
    describe: 'List or search the locally installed audio files.',
    arguments: [{ name: 'query', type: 'rest' }],
  },
  youtube: {
    describe: 'Search youtube and play the first video result.',
    arguments: [{ name: 'query', type: 'rest', required: true }],
  },
  midi: {
    describe: 'Show and adjust MIDI player arguments.',
    subcommands: {
      soundfont: {
        describe: 'Get or set the MIDI soundfont.',
        arguments: [{ name: 'file', type: 'rest' }],
        impl: async (bot: IBot, [file]: string[]): Promise<string> => {
          if (file) {
            const [path, size] = await bot.setSoundfont(file);
            const cvtSize = convertSize(size);
            return `Soundfont set to: "${path}" (Size: ${cvtSize})`;
          } else {
            const [path, size] = await bot.getSoundfont();
            const cvtSize = convertSize(size);
            return `Current soundfont: "${path}" (Size: ${cvtSize})`;
          }
        }
      },
      listsoundfonts: {
        describe: 'List or search the locally installed soundfonts.',
        arguments: [{ name: 'query', type: 'rest' }],
      }
    }
  }
};

