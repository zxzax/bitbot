// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2021 Jason Francis <zxzax@protonmail.com>

mod sys;

use self::sys::*;
use std::ffi::CStr;
use std::fmt;
use std::os::raw::c_int;

#[derive(fmt::Debug)]
pub struct Error(i32);

impl Error {
    fn new(code: i32) -> Error {
        Error(code)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        let cstr = unsafe { CStr::from_ptr(opus_strerror(self.0)) };
        fmt.write_str(cstr.to_string_lossy().as_ref())
    }
}

impl std::error::Error for Error {}

pub struct Encoder(*mut OpusEncoder);

impl Encoder {
    pub fn new() -> Result<Encoder, Error> {
        let mut error = OPUS_OK as i32;
        let enc = unsafe { opus_encoder_create(
                48000, 1, OPUS_APPLICATION_AUDIO as i32,
                &mut error as *mut c_int) };
        if enc.is_null() {
            Err(Error::new(error))
        } else {
            Ok(Encoder(enc))
        }
    }
    pub fn encode_mono(&mut self,
                       input: &[i16; 240],
                       output: &mut [u8]
                      ) -> Result<usize, Error> {
        debug_assert!(output.len() <= i32::MAX as usize);
        let res = unsafe {
            opus_encode(self.0,
                        input.as_ptr(), input.len() as i32,
                        output.as_mut_ptr(), output.len() as i32)
        };
        if res < 0 {
            Err(Error::new(res))
        } else {
            Ok(res as usize)
        }
    }
}

impl Drop for Encoder {
    fn drop(&mut self) {
        unsafe { opus_encoder_destroy(self.0) };
    }
}

