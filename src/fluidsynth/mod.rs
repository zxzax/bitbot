// SPDX-License-Identifier: MIT
// SPDX-FileCopyrightText: 2021 Jason Francis <zxzax@protonmail.com>

mod sys;

use serde::Deserialize;
use self::sys::*;
use std::ffi::{CStr, c_void};
use std::fmt::{self, Write};
use std::mem::MaybeUninit;

#[derive(fmt::Debug)]
pub struct Error(String);

impl Error {
    fn new(message: String) -> Error {
        Error(message)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt.write_str(self.0.as_ref())
    }
}

impl std::error::Error for Error {}

pub type Result<T> = std::result::Result<T, Error>;

extern fn handler(_level: i32, message: *const i8, data: *mut c_void) {
    let buffer = unsafe { &mut *(data as *mut String) };
    let cstr = unsafe { CStr::from_ptr(message) };
    buffer.write_str(cstr.to_string_lossy().as_ref()).unwrap();
}

fn with_error_handler<T, F>(func: F) -> Result<T>
    where F: FnOnce() -> Option<T> {
    let mut buffer = String::new();
    let buffer_ptr = &mut buffer as *mut String as *mut c_void;
    let (old_panic, old_err) = unsafe {
        (fluid_set_log_function(
            fluid_log_level_FLUID_PANIC as i32, Some(handler),
            buffer_ptr),
        fluid_set_log_function(
            fluid_log_level_FLUID_ERR as i32, Some(handler),
            buffer_ptr))
    };
    let res = func();
    unsafe {
        fluid_set_log_function(
            fluid_log_level_FLUID_PANIC as i32, old_panic, std::ptr::null_mut());
        fluid_set_log_function(
            fluid_log_level_FLUID_ERR as i32, old_err, std::ptr::null_mut());
    }
    res.ok_or_else(|| Error::new(buffer))
}

fn for_ptr_with_error_handler<T, F>(func: F) -> Result<*mut T>
    where F: FnOnce() -> *mut T {
    with_error_handler(|| {
        let ptr = func();
        if ptr.is_null() { None } else { Some(ptr) }
    })
}

fn for_int_with_error_handler<F>(func: F) -> Result<i32>
    where F: FnOnce() -> i32 {
    with_error_handler(|| {
        let code = func();
        if code == FLUID_FAILED { None } else { Some(code) }
    })
}

fn for_code_with_error_handler<F>(func: F) -> Result<()>
    where F: FnOnce() -> i32 {
    for_int_with_error_handler(func).map(|_| ())
}

#[derive(Deserialize)]
pub enum Setting {
    Int(i32),
    Num(f64),
    Str(String)
}

pub struct Settings(*mut _fluid_hashtable_t);

impl Settings {
    pub fn new() -> Result<Settings> {
        let settings = for_ptr_with_error_handler(|| unsafe {
            new_fluid_settings()
        })?;
        Ok(Settings(settings))
    }
    pub fn set(&mut self, key: &String, value: &Setting) -> Result<()> {
        for_code_with_error_handler(|| {
            let keystr = key.as_ptr() as *const i8;
            match value {
                Setting::Int(v) => unsafe {
                    fluid_settings_setint(self.0, keystr, *v)
                },
                Setting::Num(v) => unsafe {
                    fluid_settings_setnum(self.0, keystr, *v)
                },
                Setting::Str(v) => unsafe {
                    fluid_settings_setstr(self.0, keystr, v.as_ptr() as *const i8)
                }
            }
        })
    }
}

impl Drop for Settings {
    fn drop(&mut self) {
        unsafe { delete_fluid_settings(self.0) };
    }
}

pub struct Synth {
    synth: *mut _fluid_synth_t,
    _settings: Settings
}

impl Synth {
    pub fn new(settings: Settings) -> Result<Synth> {
        let synth = for_ptr_with_error_handler(|| unsafe {
            new_fluid_synth(settings.0)
        })?;
        Ok(Synth{synth, _settings: settings})
    }
    pub fn sfload(&mut self, filename: &String, reset_presets: bool) -> Result<i32> {
        for_int_with_error_handler(|| unsafe {
            fluid_synth_sfload(self.synth,
                               filename.as_ptr() as *const i8,
                               reset_presets as i32)
        })
    }
    pub fn sfunload(&mut self, id: i32, reset_presets: bool) -> Result<()> {
        for_code_with_error_handler(|| unsafe {
            fluid_synth_sfunload(self.synth, id, reset_presets as i32)
        })
    }
    pub fn write_s16<'a>(&mut self,
                         lout: &mut [i16; 240],
                         rout: &'a mut MaybeUninit<[i16; 240]>,
                         ) -> Result<&'a [i16; 240]> {
        for_code_with_error_handler(|| unsafe {
            fluid_synth_write_s16(
                self.synth, lout.len() as i32,
                lout.as_mut_ptr() as *mut c_void, 0, 1,
                rout.as_mut_ptr() as *mut c_void, 0, 1)
        })?;
        Ok(unsafe { rout.assume_init_ref() })
    }
}

impl Drop for Synth {
    fn drop(&mut self) {
        unsafe { delete_fluid_synth(self.synth) };
    }
}

pub struct Player {
    player: *mut _fluid_player_t,
    synth: Synth
}

impl Player {
    pub fn new(synth: Synth) -> Result<Player> {
        let player = for_ptr_with_error_handler(|| unsafe {
            new_fluid_player(synth.synth)
        })?;
        Ok(Player { player, synth })
    }
    pub fn mut_synth(&mut self) -> &mut Synth {
        return &mut self.synth;
    }
}

impl Drop for Player {
    fn drop(&mut self) {
        unsafe { delete_fluid_player(self.player) };
    }
}

